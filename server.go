package droptable

import (
	"bytes"
	"context"
	"crypto/md5"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

func logErr(err error) {
	fmt.Fprintf(os.Stderr, "%+v\n", err)
}

type StorageService struct {
	port    string
	table   *Table
	loggers map[string]*log.Logger
}

func NewStorageService(dir, port string) (s *StorageService, err error) {
	s = new(StorageService)
	s.table = new(Table)
	if err = s.table.Init(dir); err != nil {
		return nil, err
	}

	s.port = port

	// create distinct logger for each endpoint
	s.loggers = map[string]*log.Logger{
		"duplicates": log.New(os.Stdout, "/duplicates: ", 0),
		"create":     log.New(os.Stdout, "/create: ", 0),
		"delete":     log.New(os.Stdout, "/delete: ", 0),
	}
	return s, nil
}

func (s *StorageService) Serve(wait time.Duration) (err error) {
	if s.port == "" {
		// default port entry
		s.port = "8080"
	}
	addr := fmt.Sprintf("localhost:%s", s.port)

	log.Printf("Starting notebook server on %s\n", addr)
	r := mux.NewRouter()

	srv := &http.Server{
		Handler:      r,
		Addr:         addr,
		WriteTimeout: 2 * time.Second,
		ReadTimeout:  2 * time.Second,
	}

	r.HandleFunc("/cat/{filename}", s.Cat).Methods("GET")
	r.HandleFunc("/create/{filename}", s.Create).Methods("PUT")
	r.HandleFunc("/delete/{filename}", s.Delete).Methods("DELETE")
	r.HandleFunc("/duplicates", s.Duplicates).Methods("GET")
	r.HandleFunc("/ls", s.Ls).Methods("GET")
	r.HandleFunc("/ls/{obj}", s.Ls).Methods("GET")
	r.HandleFunc("/ls/{obj}/{opts}", s.Ls).Methods("GET")

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	// graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	srv.Shutdown(ctx)
	log.Println("shutting down")
	return nil

}

// Ls returns the IdxFile slice as a string
func (s *StorageService) Ls(w http.ResponseWriter, r *http.Request) {
	var returnJson interface{}
	var details bool
	var b []byte
	var err error

	obj, ok := mux.Vars(r)["obj"]
	if !ok {
		obj = "files"
	}

	opts, _ := mux.Vars(r)["opts"]
	if opts == "details" {
		details = true
	}

	switch obj {
	case "":
		fallthrough
	case "files":
		if !details { // return a newline delimited string slice
			files := []string{}
			for _, f := range s.table.files {
				files = append(files, f.Name)
			}
			b = []byte(strings.Join(files, "\n"))
		} else {
			returnJson = s.table.files
		}
	case "chunkMap":
		if !details {
			chunkValues := []string{}
			for k := range s.table.chunkMap {
				chunkValues = append(chunkValues, fmt.Sprintf("%x", k))
				b = []byte(strings.Join(chunkValues, "\n"))
			}
		} else {
			returnJson = s.table.ChunkMapToPayload()
		}
	}

	if details {
		// marshal as json so we can handle varying object structures
		b, err = json.MarshalIndent(returnJson, "", "  ")
		if err != nil {
			logErr(err)
			return
		}
	}

	// end with newline
	b = append(b, []byte("\n")...)
	w.WriteHeader(http.StatusOK)
	w.Write(b)

}

// Duplicates returns all existing checksums tied to stored data
func (s *StorageService) Duplicates(w http.ResponseWriter, r *http.Request) {
	lgr := s.loggers["duplicates"]
	var payload bytes.Buffer
	var hashes [][md5.Size]byte

	log.Println("Processing duplicates endpoint")

	dec := gob.NewDecoder(r.Body)
	enc := gob.NewEncoder(&payload)

	if err := dec.Decode(&hashes); err != nil {
		logErr(err)
		return
	}
	lgr.Println("Got leafHash payload with len: ", len(hashes))
	lgr.Printf("Hashes: %x\n", hashes)
	dupes := s.table.Duplicates(hashes)

	lgr.Printf("Returning duplicates: %v\n", dupes)
	payload.Reset()
	if err := enc.Encode(&dupes); err != nil {
		logErr(err)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(payload.Bytes())

}

// Cat returns the bytes of a given IdxFile
func (s *StorageService) Cat(w http.ResponseWriter, r *http.Request) {
	filename, ok := mux.Vars(r)["filename"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	b, err := s.table.getFileData(filename)
	if err != nil {
		logErr(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(b)

}

func (s *StorageService) Create(w http.ResponseWriter, r *http.Request) {
	var plChunks [][]PayloadChunk
	var chunks []*Chunk
	lgr := s.loggers["create"]

	filename, ok := mux.Vars(r)["filename"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	lgr.Println("Processing create endpoint for file: ", filename)

	dec := gob.NewDecoder(r.Body)
	if err := dec.Decode(&plChunks); err != nil {
		logErr(err)
		return
	}

	for _, c := range plChunks {
		chunks = append(chunks, FromPayload(c))
	}
	lgr.Printf("Chunks to create: %v\n", chunks)
	if err := s.table.Create(chunks, filename); err != nil {
		logErr(err)
		return
	}

	w.WriteHeader(http.StatusOK)

}

func (s *StorageService) Delete(w http.ResponseWriter, r *http.Request) {
	var filepath string
	lgr := s.loggers["delete"]

	filename, ok := mux.Vars(r)["filename"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	lgr.Println("Processing delete endpoint for file: ", filename)

	dec := gob.NewDecoder(r.Body)
	if err := dec.Decode(&filepath); err != nil {
		logErr(err)
		return
	}

	if err := s.table.Delete(filename); err != nil {
		logErr(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("%q deleted", filepath)))

}
