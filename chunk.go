package droptable

import (
	"crypto/md5"
	"fmt"
	"os"

	"github.com/pkg/errors"
)

type ByteUnit int64

const (
	_           = iota // ignore first value by assigning to blank identifier
	KB ByteUnit = 1 << (10 * iota)
	MB
	GB
)

// Chunk represents the binary nodes referencing the data ranges used to reproduce a file
type Chunk struct {
	Data  []byte
	Sum   [md5.Size]byte
	Left  *Chunk
	Right *Chunk
}

// NewChunk creates a new file from a byte slice
func NewChunk(b []byte) *Chunk {
	return &Chunk{
		Data: b,
		Sum:  md5.Sum(b),
	}
}

func ChunkFromFile(filename string) (*Chunk, error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return splitData(b), nil

}

// ChunkToFile creates a a file of the non-recursive data held by a Chunk
func (c *Chunk) ToFile(filename string) error {
	return os.WriteFile(filename, c.Data, 0666) // rw-rw-rw-
}

func (c Chunk) String() string {
	var childStr string
	if c.Left != nil && c.Right != nil {
		childStr = fmt.Sprintf("-LeftSum- %x -RightSum- %x", c.Left.Sum, c.Right.Sum)
	}
	return fmt.Sprintf(`[ChunkSum- %x %s]`, c.Sum, childStr)
}

func (c *Chunk) HasChildren() bool {
	return c.Left != nil && c.Right != nil
}

// HasLeafChildrenWithData returns true if the given node has
// two leaf nodes with both possessing data
func (c *Chunk) HasLeafChildrenWithData() bool {
	switch {
	case !c.HasChildren():
		return false
	case c.Left.Data != nil && c.Right.Data != nil:
		return true
	}
	return false
}

// GetParentBySum finds the parent node that has a left or right chunk with the given sum
func (c *Chunk) Deduplicate(dupes [][md5.Size]byte) {
	if c.HasChildren() {
		c.Left.Deduplicate(dupes)
		c.Right.Deduplicate(dupes)
	}

	// Ignore []*Chunk with no data and no children
	if c.Data == nil {
		return
	}

	for _, sum := range dupes {
		if c.Sum == sum {
			c.Data = nil
			return
		}
	}
}

// Balance mutates a Chunk object ensuring that left data is bisected into
// left and right Chunk nodes
func (c *Chunk) Balance() {
	switch {
	case !c.HasChildren():
		return
	case c.Left.Data != nil && c.Right.Data == nil:
		c.Left.Split()
	case c.Left.Data == nil && c.Right.Data != nil:
		c.Right.Split()
	default:
		c.Left.Balance()
		c.Right.Balance()
	}
}

// PayloadChunk is the pointerless object used as a payload in Client.Create
type PayloadChunk struct {
	Data []byte
	Sum  [md5.Size]byte
}

// Payload returns a Chunk slice with a max depth of 2 represented as a DataChunk
// an entry will be either a duplicate chunk (representing data already present on the server)
// or a bisected chunk holding data to be sent and added
func (c *Chunk) Payload() (pl [][]PayloadChunk) {
	switch {
	case c.HasLeafChildrenWithData():
		data := []PayloadChunk{
			{Data: []byte{}, Sum: c.Sum},
			{Data: c.Left.Data, Sum: c.Left.Sum},
			{Data: c.Right.Data, Sum: c.Right.Sum},
		}
		return append(pl, data)
	case !c.HasChildren(): // a leaf node with no data
		data := []PayloadChunk{{[]byte{}, c.Sum}}
		return append(pl, data)
	}

	pl = append(pl, c.Left.Payload()...)
	pl = append(pl, c.Right.Payload()...)
	return pl
}

func FromPayload(pl []PayloadChunk) *Chunk {
	if len(pl) == 1 {
		c := pl[0]
		return &Chunk{
			Data: nil,
			Sum:  c.Sum,
		}
	}

	// main chunk will always be index 0
	// left chunk will always be index 1
	// right chunk will always be index 2
	c := pl[0]
	l := pl[1]
	r := pl[2]

	return &Chunk{
		Data: nil,
		Sum:  c.Sum,
		Left: &Chunk{
			Data: l.Data,
			Sum:  l.Sum,
		},
		Right: &Chunk{
			Data: r.Data,
			Sum:  r.Sum,
		},
	}
}

// GetLeafHashes returns the ordered list of leaf node hashes with non nil data
func (c *Chunk) GetLeafHashes(opts ...string) [][md5.Size]byte {
	sums := [][md5.Size]byte{}
	cfg := struct {
		getEmpty bool
	}{false}
	// return nodes without data if "get_empty" is passed in
	for _, v := range opts {
		if v == "get_empty" {
			cfg.getEmpty = true
		}
	}

	if c.HasChildren() {
		sums = append(sums, c.Left.GetLeafHashes()...)
		sums = append(sums, c.Right.GetLeafHashes()...)
		return sums
	}

	// Do not return hash of Chunk with empty data by default
	if c.Data != nil || cfg.getEmpty {
		return append(sums, c.Sum)
	}
	return sums
}

// GetData returns all of the data contained in a Chunk
func (c *Chunk) GetData() (data []byte) {
	if c.HasChildren() {
		data = append(data, c.Left.GetData()...)
		data = append(data, c.Right.GetData()...)
		return data
	}
	return append(data, c.Data...)
}

// Split bisects the given Chunk's data
func (c *Chunk) Split() {
	*c = *splitData(c.Data)
}

// partSize returns the index that partitions byteAmt into a value divisible by 2
// starting with the relevant ByteUnit
func partSize(byteLen int, roof ByteUnit) int {
	split := int(roof)
	for byteLen < split {
		split /= 2
	}
	return split
}

func splitData(b []byte) *Chunk {
	var splitIdx int
	size := len(b)
	switch {
	case size == 0:
		return &Chunk{nil, emptySum, nil, nil}
	case size < int(KB):
		splitIdx = partSize(size, KB)
	case size < int(MB):
		splitIdx = partSize(size, MB)
	case size < int(GB):
		splitIdx = partSize(size, GB)
	default:
		panic("file size greater than 1GB")
	}
	return &Chunk{
		Data:  nil,
		Sum:   md5.Sum(b),
		Left:  NewChunk(b[:splitIdx]),
		Right: NewChunk(b[splitIdx:]),
	}
}
