package droptable

import (
	"bufio"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
)

var dataSubdir = "data"

// equivalent to `echo -n "" | md5sum`, could be made a const
var emptySum = md5.Sum([]byte{})

// IdxRow is a map type holding key maps to an existing stored chunk
// with values index 0 and index 1 mapping to potentially unrealised checksums
// if the data was to be bisected
type IdxRow struct {
	head, left, right [md5.Size]byte
}

func (r *IdxRow) toSlice() [][md5.Size]byte {
	return [][md5.Size]byte{r.head, r.left, r.right}
}

func (r *IdxRow) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("[\"%x\",\"%x\",\"%x\"]", r.head, r.left, r.right)), nil
}

// rowsToIdxFile writes slice []IdxRow to a file using newline delimited stringified
// hexadecimal hash values of each given IdxRow
func rowsToIdxFile(filename string, rows []IdxRow) error {
	var b []byte

	ok, err := canWrite(filename)
	if !ok {
		return nil
	} else if err != nil {
		return errors.WithStack(err)
	}

	for _, row := range rows {
		b = append(b, []byte(fmt.Sprintf("%x %x %x\n", row.head, row.left, row.right))...)
	}
	if err := os.WriteFile(filename, b, 0666); err != nil { // rw-rw-rw-
		return errors.WithStack(err)
	}
	return nil
}

func readFileToIdxRows(filename string) (rows []IdxRow, err error) {
	r, err := os.Open(filename)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	s := bufio.NewScanner(r)
	for s.Scan() {
		line := s.Text()
		sums := strings.Split(line, " ")
		if len(sums) != 3 {
			return nil, errors.Errorf("%s is not an MD5 sum", s.Text())
		}

		// handle checksum triplicate mismatch
		var row IdxRow
		toHead, _ := hex.DecodeString(sums[0])
		toLeft, _ := hex.DecodeString(sums[1])
		toRight, _ := hex.DecodeString(sums[2])
		copy(row.head[:], toHead)
		copy(row.left[:], toLeft)
		copy(row.right[:], toRight)
		rows = append(rows, row)
	}

	return rows, err
}

// IdxFile holds references to a file name and rows holding bisected data
type IdxFile struct {
	Name string   `json:"name"`
	Rows []IdxRow `json:"rows"`
}

func (f *IdxFile) Unmarshal(filename string) (err error) {
	f.Rows, err = readFileToIdxRows(filename)
	return err
}

type Table struct {
	dir   string
	files []IdxFile
	// maps md5 sums to filenames
	chunkMap map[[md5.Size]byte]set
}

func (t *Table) ChunkMapToPayload() map[string][]string {
	listMap := make(map[string][]string)
	for k, v := range t.chunkMap {
		// give stringified version of hash as key
		listMap[fmt.Sprintf("%x", k)] = v.keys()
	}
	return listMap
}

// Init starts the server directory where data chunks and file references are stored
func (t *Table) Init(dir string) error {
	t.dir = dir
	t.chunkMap = make(map[[md5.Size]byte]set)
	var objDir fs.DirEntry

	log.Println("InitTable for dir: ", dir)

	entries, err := os.ReadDir(dir)
	if err != nil {
		return errors.WithStack(err)
	}

	// create file indices
	for _, entry := range entries {
		fidx := IdxFile{}

		// check for data dir here
		if entry.Name() == dataSubdir {
			objDir = entry
			if !objDir.IsDir() {
				return errors.New(filepath.Join(dir, objDir.Name()+" is not a directory"))
			}
			continue
		}

		fidx.Name = entry.Name()
		filename := filepath.Join(dir, entry.Name())
		if entry.IsDir() {
			continue
		}

		if err = fidx.Unmarshal(filename); err != nil {
			return errors.WithStack(err)
		}

		// add filename to chunkMap
		t.mapFilename(fidx.Name, fidx.Rows)

		t.files = append(t.files, fidx)
	}

	// create chunkMap references
	switch {
	case len(t.files) != 0 && objDir == nil:
		return errors.New(dir + ": files exist without data subdirectory")
	case len(t.files) != 0 && objDir != nil:
		objFiles, err := os.ReadDir(filepath.Join(dir, objDir.Name()))
		if err != nil {
			return errors.WithStack(err)
		}

		// ensure md5sums derived from file indices are present in the data subdir
		for sum := range t.chunkMap {
			strSum := fmt.Sprintf("%x", sum)

			match := false
			for _, entry := range objFiles {
				if strSum == entry.Name() {
					match = true
					break
				}
			}
			if !match {
				return errors.Errorf("%x missing from %s", sum, objDir.Name())
			}
		}
	case len(t.files) == 0 && objDir != nil:
		log.Println("data subdirectory exists without file indices")
	case objDir == nil:
		if err = os.Mkdir(filepath.Join(dir, dataSubdir), 0777); err != nil {
			return errors.WithStack(err)
		}
	}

	// create file with empty sum if it does not exist
	emptySumFile := t.chunkFilename(emptySum)
	if _, err := os.Stat(emptySumFile); os.IsNotExist(err) {
		if err = os.WriteFile(emptySumFile, []byte(""), 0666); err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

func (t *Table) Duplicates(hashes [][md5.Size]byte) (dupes [][md5.Size]byte) {
	for _, sum := range hashes {
		if _, ok := t.chunkMap[sum]; ok {
			dupes = append(dupes, sum)
		}
	}
	return dupes
}

func (t *Table) Create(chunks []*Chunk, filename string) (err error) {
	var fileRows []IdxRow

	for _, c := range chunks {
		if c.HasLeafChildrenWithData() {
			// 1. write parent chunk first
			chunkRows := []IdxRow{{c.Sum, c.Left.Sum, c.Right.Sum}}
			if err = rowsToIdxFile(t.chunkFilename(c.Sum), chunkRows); err != nil {
				return errors.WithStack(err)
			}
			// 2. set parent chunk as read only
			if err = setReadOnly(t.chunkFilename(c.Sum)); err != nil {
				return errors.WithStack(err)
			}
			// 3. write child chunks
			chunkName := t.chunkFilename(c.Left.Sum)
			if ok, _ := canWrite(chunkName); ok {
				if err = c.Left.ToFile(t.chunkFilename(c.Left.Sum)); err != nil {
					return errors.WithStack(err)
				}
			}
			chunkName = t.chunkFilename(c.Right.Sum)
			if ok, _ := canWrite(chunkName); ok {
				if err = c.Right.ToFile(t.chunkFilename(c.Right.Sum)); err != nil {
					return errors.WithStack(err)
				}
			}

			fileRows = append(fileRows, chunkRows...)

			// add file references to chunkMap
			t.mapFilename(filename, chunkRows)
		} else if c.Sum != emptySum { // else treat as duplicate chunk
			var dupeRows []IdxRow
			log.Printf("Duplicate chunk: %x\n", c.Sum)
			dupeFilename := t.chunkFilename(c.Sum)
			if dupeRows, err = readFileToIdxRows(dupeFilename); err != nil {
				return err
			}
			if len(dupeRows) != 1 {
				return errors.Errorf("%q returned rows with length of %d", dupeFilename, len(dupeRows))
			}

			fileRows = append(fileRows, dupeRows[0])

		}
	}
	if err := rowsToIdxFile(path.Join(t.dir, filename), fileRows); err != nil {
		return errors.WithStack(err)
	}
	t.files = append(t.files, IdxFile{Name: filename, Rows: fileRows})
	return nil
}

func (t *Table) Delete(filename string) error {
	// 1. remove FileIdx from Table
	idxFileFound := false
	for i, v := range t.files {
		if v.Name == filename {
			// pop value out of slice
			t.files = append(t.files[:i], t.files[i+1:]...)
			idxFileFound = true
		}
	}
	if !idxFileFound {
		return errors.Errorf("%q was not found in Table.IdxFiles", filename)
	}
	// 2. remove all chunkMap references to the file
	for _, v := range t.chunkMap {
		v.remove(filename)
	}
	// 3. remove chunk files if their reference slice is empty after v.remove is done
	for k, v := range t.chunkMap {
		if len(v.keys()) != 0 {
			continue
		}
		// delete map key + values
		delete(t.chunkMap, k)
		// delete chunk file
		if err := os.Remove(t.chunkFilename(k)); err != nil {
			return errors.WithStack(err)
		}
	}

	// 4. remove file proper once the map and slice are cleaned
	if err := os.Remove(path.Join(t.dir, filename)); err != nil {
		return errors.WithStack(err)
	}

	return nil

}

func (t *Table) chunkFilename(chunk [md5.Size]byte) string {
	return path.Join(t.dir, dataSubdir, fmt.Sprintf("%x", chunk))

}

// mapFilename adds the given filename to all keys in chunkMap within the 2D rows slice
func (t *Table) mapFilename(filename string, rows []IdxRow) {
	for _, row := range rows {
		for _, sum := range row.toSlice() {
			if _, ok := t.chunkMap[sum]; !ok {
				t.chunkMap[sum] = make(set)
			}
			t.chunkMap[sum].add(filename)
		}
	}
}

// splitDataFile splits a data file, leaving a reference to the child data files in its place
// NOTE disabled due to not backpropagating new SHAs to old files
func (t *Table) splitDataFile(filename string) (*Chunk, error) {
	// 1. create chunk from file
	parentChunk, err := ChunkFromFile(filename)
	if err != nil {
		return nil, err
	}
	// 2. split chunk
	parentChunk.Split()
	// 3. write child chunks
	if err = parentChunk.Left.ToFile(t.chunkFilename(parentChunk.Left.Sum)); err != nil {
		return nil, err
	}
	if err = parentChunk.Right.ToFile(t.chunkFilename(parentChunk.Right.Sum)); err != nil {
		return nil, err
	}
	// 4. update parent chunk last to avoid data loss
	chunkRows := []IdxRow{{parentChunk.Sum, parentChunk.Left.Sum, parentChunk.Right.Sum}}
	if err = rowsToIdxFile(t.chunkFilename(parentChunk.Sum), chunkRows); err != nil {
		return nil, err
	}
	// 5. set parent chunk as read only
	if err = setReadOnly(t.chunkFilename(parentChunk.Sum)); err != nil {
		return nil, err
	}

	// 6. add parent chunk file refs to child file refs
	for _, sum := range parentChunk.GetLeafHashes() {
		if _, ok := t.chunkMap[sum]; !ok {
			t.chunkMap[sum] = make(set)
		}
		t.chunkMap[sum].add(t.chunkMap[parentChunk.Sum].keys()...)
	}

	return parentChunk, nil
}

func (t *Table) getData(sum [md5.Size]byte) (data []byte, err error) {
	filename := t.chunkFilename(sum)

	// if filename is readonly, assume it is an IdxFile
	if ok, _ := canWrite(filename); !ok {
		idxFile := new(IdxFile)
		idxFile.Unmarshal(filename)
		for _, row := range idxFile.Rows {
			leftData, err := t.getData(row.left)
			if err != nil {
				return nil, err
			}
			rightData, err := t.getData(row.right)
			if err != nil {
				return nil, err
			}

			data = append(data, leftData...)
			data = append(data, rightData...)
		}
		return data, nil

	}

	// return data for a file with RW permissions
	if data, err = os.ReadFile(t.chunkFilename(sum)); err != nil {
		return nil, errors.WithStack(err)
	}

	return data, nil
}

func (t *Table) getFileData(filename string) (data []byte, err error) {
	var found bool
	var file IdxFile

	// search for filenames in the Table object
	for _, f := range t.files {
		if filename == f.Name {
			file = f
			found = true
		}
	}
	if !found {
		return nil, errors.Errorf("%q is not present in the directory", filename)
	}

	// return data for left and right child nodes
	// a file with readonly permissions only holds two pointers to data in r.left and r.right
	// for the relevant data
	for _, r := range file.Rows {
		leftBytes, err := t.getData(r.left)
		if err != nil {
			return nil, err
		}
		rightBytes, err := t.getData(r.right)
		if err != nil {
			return nil, err
		}

		data = append(data, leftBytes...)
		data = append(data, rightBytes...)

	}
	return data, nil

}

func setReadOnly(filepath string) error {
	err := os.Chmod(filepath, 0444)
	return errors.WithStack(err)
}

// canWrite returns true if the given file has RW permissions
func canWrite(filepath string) (bool, error) {
	file, err := os.OpenFile(filepath, os.O_WRONLY, 0665)
	if err != nil {
		if os.IsPermission(err) {
			return false, err
		}
	}
	file.Close()
	return true, nil
}

// poor man's hash set
type set map[string]bool

func (s set) add(strings ...string) {
	for _, str := range strings {
		s[str] = true
	}
}

func (s set) remove(str string) {
	if _, ok := s[str]; ok {
		delete(s, str)
	}
}

func (s set) has(str string) bool {
	_, ok := s[str]
	return ok
}

func (s set) keys() (keys []string) {
	for k := range s {
		keys = append(keys, k)
	}
	return keys
}
