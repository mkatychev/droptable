# droptable
A dropbox clone


## Getting started

```
Droptable like dropbox, sort of

Usage:
  droptable watch <dir> -p <port>
  droptable serve <dir> -p <port>

Options:
  -h --help        Show this screen.
  --version        Show version.
  -p <port>        Listening port for server.
```

1. Build: `go build -o $GOPATH/bin ./cmd/droptable`
2. Run directory watcher: `mkdir ./watch_dir; droptable ./watch_dir -p 8081`
3. Run virtual file storage: `mkdir ./file_server; droptable ./file_server -p 8081`

## Interactive with the file server

1. Retrieving a file: `curl -s localhost:8081/cat/file_name.here > /file/output.here`
2. Querying Stored Files: 

```sh
curl -s localhost:8081/cat/file_name.here > /file/output.here # downloads a single file to the given location
curl -s localhost:8081/ls                                     # returns all files present in the server directory
curl -s localhost:8081/ls/files                               # returns all files present in the server directory
curl -s localhost:8081/ls/files/details                       # returns all files present in the server directory with hash chunks referenced
curl -s localhost:8081/ls/chunkMap                            # returns all chunk data present in the server directory
curl -s localhost:8081/ls/chunkMap/details                    # returns all chunk data present in the server directory with filenames referenced
```

