package main

import (
	"fmt"
	"os"
	"time"

	"github.com/docopt/docopt-go"
	"gitlab.com/mkatychev/droptable"
)

const usage string = `
Droptable like dropbox, sort of

Usage:
  droptable watch <dir> -p <port>
  droptable serve <dir> -p <port>

Options:
  -h --help        Show this screen.
  --version        Show version.
  -p <port>        Listening port for server.
 `

// Conf is used to bind CLI arguments and options
type Conf struct {
	Watch bool
	Serve bool
	Dir   string
	Port  string `docopt:"-p"`
}

var conf Conf

func main() {
	err := run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%+v\n", err)
		os.Exit(1)
	}
}

// run handles the main logic in parsing the CLI arguments
func run() error {

	opts, err := docopt.ParseArgs(usage, os.Args[1:], "0.1")
	if err != nil {
		return err
	}

	if err = opts.Bind(&conf); err != nil {
		return err
	}

	switch {
	case conf.Watch:
		if err = droptable.Watch(conf.Dir, conf.Port); err != nil {
			return err
		}
	case conf.Serve:
		service, err := droptable.NewStorageService(conf.Dir, conf.Port)
		if err != nil {
			return err
		}
		if err = service.Serve(5 * time.Second); err != nil {
			return err
		}
	}

	return nil
}
