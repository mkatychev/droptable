package droptable

import (
	"bytes"
	"crypto/md5"
	"encoding/gob"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"path"
	"path/filepath"

	"github.com/pkg/errors"
)

type Client struct {
	host *url.URL
	// event
}

func NewClient(port string) (c *Client, err error) {
	addr := fmt.Sprintf("http://localhost:%s/", port)
	c = new(Client)
	if c.host, err = url.Parse(addr); err != nil {
		return nil, errors.WithStack(err)
	}
	return c, nil
}

func (c *Client) join(paths ...string) string {
	endpoint := path.Join(paths...)
	return c.host.String() + endpoint
}

// TrimChunk will continually deduplicate and balance the given chunk using the duplicate slices returns by the
// file server
func TrimChunk(urlPath string, chunk *Chunk) (*Chunk, error) {
	var payload bytes.Buffer
	var dupes [][md5.Size]byte

	if chunk.Data == nil {
		return chunk, nil
	}

	chunk.Split()

	for {
		sums := chunk.GetLeafHashes()
		enc := gob.NewEncoder(&payload)
		dec := gob.NewDecoder(&payload)

		if err := enc.Encode(sums); err != nil {
			return nil, errors.WithStack(err)
		}

		log.Println("getting dupes...")
		request, err := http.NewRequest("GET", urlPath, &payload)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		response, err := http.DefaultClient.Do(request)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		payload.Reset()
		io.Copy(&payload, response.Body)
		defer response.Body.Close()

		if err = dec.Decode(&dupes); err != nil {
			return nil, errors.WithStack(err)
		}

		// if no more duplicate hashes are returned, break the loop
		if len(dupes) == 0 {
			break
		}

		chunk.Deduplicate(dupes)
		chunk.Balance()
	}

	return chunk, nil

}

// Create handles watch events when new data is written to a file
func (c *Client) Create(filename string) (err error) {
	var chunk *Chunk
	var payload bytes.Buffer
	urlPath := c.join("create", filepath.Base(filename))

	if chunk, err = ChunkFromFile(filename); err != nil {
		return err
	}
	if chunk, err = TrimChunk(c.join("duplicates"), chunk); err != nil {
		return err
	}

	enc := gob.NewEncoder(&payload)
	if err = enc.Encode(chunk.Payload()); err != nil {
		return errors.WithStack(err)
	}

	request, err := http.NewRequest("PUT", urlPath, &payload)
	if err != nil {
		return errors.WithStack(err)
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return errors.WithStack(err)
	}

	payload.Reset()
	io.Copy(&payload, response.Body)
	defer response.Body.Close()

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		return errors.Errorf("%q: returned status code of %d: %s", urlPath, response.StatusCode, payload.Bytes())
	}

	return errors.WithStack(err)
}

// Delete is the method used for when the watcher renames or removes a file
func (c *Client) Delete(filename string) (err error) {
	var payload bytes.Buffer
	urlPath := c.join("delete", filepath.Base(filename))

	enc := gob.NewEncoder(&payload)
	if err = enc.Encode(filename); err != nil {
		return errors.WithStack(err)
	}

	request, err := http.NewRequest("DELETE", urlPath, &payload)
	if err != nil {
		return errors.WithStack(err)
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return errors.WithStack(err)
	}

	payload.Reset()
	io.Copy(&payload, response.Body)
	defer response.Body.Close()

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		return errors.Errorf("%q: returned status code of %d: %s", urlPath, response.StatusCode, payload.Bytes())
	}

	return errors.WithStack(err)
}
