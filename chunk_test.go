package droptable

import (
	"crypto/md5"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeduplicate(t *testing.T) {
	data1 := []byte("data1")
	data1sum := md5.Sum(data1)
	data2 := []byte("data2")
	data2sum := md5.Sum(data2)
	dupe1 := []byte("dupe1")
	dupe1sum := md5.Sum(dupe1)
	dupe2 := []byte("dupe2")
	dupe2sum := md5.Sum(dupe2)
	dupes := [][md5.Size]byte{dupe1sum, dupe2sum}

	t.Run("1Dupe", func(t *testing.T) {
		chunk := &Chunk{
			Left:  &Chunk{Data: data1, Sum: data1sum},
			Right: &Chunk{Data: dupe1, Sum: dupe1sum},
		}
		chunk.Deduplicate(dupes)
		assert.Equal(t, [][md5.Size]byte{data1sum}, chunk.GetLeafHashes())

	})
	t.Run("2Dupe", func(t *testing.T) {
		chunk := &Chunk{
			Left: &Chunk{
				Left: &Chunk{
					Data: dupe1,
					Sum:  dupe1sum,
				},
				Right: &Chunk{
					Data: data2,
					Sum:  data2sum,
				},
			},
			Right: &Chunk{
				Left: &Chunk{
					Data: dupe1,
					Sum:  dupe1sum,
				},
				Right: &Chunk{
					Data: data1,
					Sum:  data1sum,
				},
			},
		}
		chunk.Deduplicate(dupes)
		assert.Equal(t, [][md5.Size]byte{data2sum, data1sum}, chunk.GetLeafHashes())

	})
}

func TestBalance(t *testing.T) {
	data1 := []byte("data1")
	data1sum := md5.Sum(data1)
	data2 := []byte("data2")
	data2sum := md5.Sum(data2)

	t.Run("1Balance", func(t *testing.T) {
		chunk := &Chunk{
			Left:  &Chunk{Data: data1, Sum: data1sum},
			Right: &Chunk{Sum: data2sum},
		}
		expected := splitData(chunk.Left.Data).GetLeafHashes()
		chunk.Balance()
		assert.Equal(t, expected, chunk.GetLeafHashes())

	})
	t.Run("2Dupe", func(t *testing.T) {
		chunk := &Chunk{
			Left: &Chunk{
				Left: &Chunk{
					Data: nil,
					Sum:  data1sum,
				},
				Right: &Chunk{
					Data: data2,
					Sum:  data2sum,
				},
			},
			Right: &Chunk{
				Left: &Chunk{
					Data: data1,
					Sum:  data1sum,
				},
				Right: &Chunk{
					Data: nil,
					Sum:  data2sum,
				},
			},
		}
		expected := append(splitData(chunk.Left.Right.Data).GetLeafHashes(), splitData(chunk.Right.Left.Data).GetLeafHashes()...)
		chunk.Balance()
		assert.Equal(t, expected, chunk.GetLeafHashes())
	})
}

func TestPayload(t *testing.T) {
	data1 := []byte("data1data1data1data1data1data1data1data1data1data1data1data1data1data1data1data1data1data1data1data1data1")
	data1sum := md5.Sum(data1)
	data2 := []byte("data2data2data2data2data2data2data2data2data2data2data2data2data2data2data2data2data2data2data2data2data2")
	data2sum := md5.Sum(data2)

	chunk := &Chunk{
		Left: &Chunk{
			Left: &Chunk{
				Data: data1,
				Sum:  data1sum,
			},
			Right: &Chunk{
				Data: data2,
				Sum:  data2sum,
			},
		},
		Right: &Chunk{
			Left: &Chunk{
				Data: nil,
				Sum:  data1sum,
				Left: &Chunk{
					Data: data1,
					Sum:  data1sum,
				},
				Right: &Chunk{
					Data: data2,
					Sum:  data2sum,
				},
			},
			Right: &Chunk{
				Data: nil,
				Sum:  data2sum,
			},
		},
	}
	expected := [][]PayloadChunk{
		{
			{[]byte{}, chunk.Left.Sum},
			{chunk.Left.Left.Data, chunk.Left.Left.Sum},
			{chunk.Left.Right.Data, chunk.Left.Right.Sum},
		},
		{
			{[]byte{}, chunk.Right.Left.Sum},
			{chunk.Right.Left.Left.Data, chunk.Right.Left.Left.Sum},
			{chunk.Right.Left.Right.Data, chunk.Right.Left.Right.Sum},
		},
		{{[]byte{}, chunk.Right.Right.Sum}},
	}
	chunk.Balance()
	assert.Equal(t, expected, chunk.Payload())
}

func TestData(t *testing.T) {
	text := "This is some text"
	chunk := splitData([]byte(text))

	pl := chunk.Payload()

	var data []byte
	for _, v := range pl {
		c := FromPayload(v)
		data = append(data, c.GetData()...)
	}
	assert.Equal(t, text, string(data))
}
