package droptable

import (
	"log"
	"os"
	"path/filepath"

	"github.com/fsnotify/fsnotify"
	"github.com/pkg/errors"
)

type Watcher struct {
	dir    string
	client *Client
}

func Watch(dir, port string) (err error) {
	log.Println("watching ", dir)

	client, err := NewClient(port)
	if err != nil {
		return err
	}

	if err = InitDir(dir, client); err != nil {
		return err
	}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return errors.WithStack(err)
	}
	defer watcher.Close()

	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				switch {
				case event.Op&fsnotify.Create == fsnotify.Create:
					log.Println("created file:", event.Name)
					if err = client.Create(event.Name); err != nil {
						done <- true
						return
					}
				case event.Op&fsnotify.Write == fsnotify.Write:
					log.Println("modified file:", event.Name)
				case event.Op&fsnotify.Remove == fsnotify.Remove ||
					event.Op&fsnotify.Rename == fsnotify.Rename:
					log.Println("(re)moved file:", event.Name)
					if err = client.Delete(event.Name); err != nil {
						done <- true
						return
					}
				default:
					log.Printf("event %s file: %q", event.Op, event.Name)
				}
			case errs, ok := <-watcher.Errors:
				if !ok {
					err = errs
					return
				}
			}
		}
	}()

	if err = watcher.Add(dir); err != nil {
		done <- true
	}
	<-done
	return err

}

// InitDir synchronises the target server with files already existing in the directory
func InitDir(dir string, client *Client) error {
	log.Println(dir, " init")
	files, err := os.ReadDir(dir)
	if err != nil {
		return errors.WithStack(err)
	}

	for _, file := range files {
		filename := filepath.Join(dir, file.Name())
		if file.IsDir() {
			continue
		}
		log.Println("InitDir:", filename)
		if err = client.Create(filename); err != nil {
			return err
		}
	}
	return nil
}
