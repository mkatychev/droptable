module gitlab.com/mkatychev/droptable

go 1.16

require (
	github.com/client9/misspell v0.3.4 // indirect
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20210314195730-07df6a141424 // indirect
)
